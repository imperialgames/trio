﻿using UnityEngine;
using System.Collections;

public class LaunchScreenController : MonoBehaviour 
{

    public GameObject Background;
    public Camera LaunchScreenCamera;
    public float Duration;

	
	void Start () 
    {
        this.instantiateBackground ();
        this.StartCoroutine(this.loadNextScene());
	}

    IEnumerator loadNextScene () 
    {
        yield return new WaitForSeconds(this.Duration);
        Application.LoadLevel(2);
    }

    void instantiateBackground () 
    {
        GameObject background = (GameObject) Instantiate(this.Background, new Vector3(), Quaternion.identity);
        float sceneHeight = this.LaunchScreenCamera.orthographicSize * 2.0f;
        SpriteRenderer renderer = background.GetComponent<SpriteRenderer> ();
        float backgroundHeight = renderer.sprite.rect.height / renderer.sprite.pixelsPerUnit;
        float scale = sceneHeight / backgroundHeight;
        background.transform.localScale = new Vector3(scale, scale, 1.0f);
    }
	
	
}
