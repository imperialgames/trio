﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Ball : MonoBehaviour {


    public float speed;
    public float speedGrowth;
    private Rigidbody2D rb;
    private Rotate rotator;
    private GameController gameController;
    public int hitScore;

    public AudioSource hit;
    public AudioSource lose;

    bool started = false;
	
	void Start () {   

        rb = GetComponent<Rigidbody2D>();
        this.gameController = GameController.GetGameController ();
        
    }

    void startMoving() 
    {
      

        float x = Random.Range(-10, 10) / 10.0f;
        float y = Random.Range(-10, 10) / 10.0f;

        Vector2 movement = new Vector2(x, y);

        rb.AddForce(movement * speed);
        this.rotator = Rotate.GetRotate();
    }

	public float Speed 
	{
		get 
		{
			return this.rb.velocity.magnitude;
		}
	}

    
    public static Ball GetBall () 
    {
        GameObject ballObject = GameObject.FindGameObjectWithTag("Ball");
        if (ballObject != null) 
        {
            return ballObject.GetComponent<Ball> ();
        } else 
        {
            return null;
        }
    }

	void Update () {
        if (SystemInfo.deviceType == DeviceType.Handheld) 
        {
            if (Input.touchCount > 0) 
            {
                this.startMoving ();
                this.started = true;
            }
        } else 
        {
            if (!started && Input.anyKey)
            {
                startMoving();
                started = true;
            }
        }




	}

    void FixedUpdate () 
    {
        if (rb.velocity.magnitude > speed + 0.01f || rb.velocity.magnitude < speed - 0.01f)
        {
            rb.velocity = rb.velocity.normalized * speed;
        }
    }

    void OnCollisionExit2D(Collision2D other)
    {
        
        if (other.gameObject.CompareTag("bar")) 
        {
            this.speed += this.speedGrowth;
			this.rotator.ConstantSpeed = this.speed * this.rotator.ConstantSpeedRelation;
			this.rotator.RotationSpeed = this.speed * this.rotator.MovementSpeedRelation;
			this.rotator.ForwardSpeed = this.speed * this.rotator.ForwardSpeedRelation;
            this.gameController.IncrementCounter (this.hitScore);
            Debug.Log(GlobalParams.Sound);
            if (GlobalParams.Sound)
                this.hit.Play ();
        }
    }
    
    void OnTriggerExit2D(Collider2D other) 
    {
        if (other.gameObject.CompareTag("Boundary")) 
        {
            this.gameController.GameOver ();
            started = false;
            if (GlobalParams.Sound)
                this.lose.Play ();
        }
    }
 
}
