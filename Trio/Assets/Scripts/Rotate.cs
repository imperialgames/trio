﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {

    private float constantSpeed;
    private float rotationSpeed;
    private float forwardSpeed;
    private int westTouchBorder;
    private int eastTouchBorder;

    public float ConstantSpeed 
    {
        get 
        {
            return this.constantSpeed;
        }
        set 
        {
            this.constantSpeed = value;
        }
    }

    public float RotationSpeed 
    {
        get 
        {
            return this.rotationSpeed;
        }
        set 
        {
            this.rotationSpeed = value;
        }
    }

    public float ForwardSpeed 
    {
        get 
        {
            return this.forwardSpeed;
        }
        set 
        {
            this.forwardSpeed = value;
        }
    }

    Rigidbody2D rb;

    private Ball ball;
    public float constantSpeedRelation;
    public float movementSpeedRelation;
    public float forwardSpeedRelation;
    private int random;

    public float ConstantSpeedRelation 
    {
        get 
        {
            return this.constantSpeedRelation;
        }
    }

    public float MovementSpeedRelation 
    {
        get 
        {
            return this.movementSpeedRelation;
        }
    }

    public float ForwardSpeedRelation 
    {
        get 
        {
            return this.forwardSpeedRelation;
        }
    }


	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        this.random = Random.Range(0, 2);
        if (this.random == 0) 
        {
            this.random = 1;
        } else 
        {
            this.random = -1;
        }

        this.ball = Ball.GetBall ();
        this.constantSpeed = ball.speed * this.constantSpeedRelation;
        this.rotationSpeed = ball.speed * this.movementSpeedRelation;
        this.forwardSpeed = ball.speed * this.forwardSpeedRelation;
        this.constantSpeed *= random;
        this.rotationSpeed *= random;
        this.forwardSpeed *= random;
        this.constantSpeedRelation *= random;
        this.movementSpeedRelation *= random;
        this.forwardSpeedRelation *= random;

//        this.constantSpeedRelation = this.constantSpeed / this.ball.speed;
//        Debug.Log("Constant speed: " + this.constantSpeed);
//        this.movementSpeedRelation = this.rotationSpeed / this.ball.speed;
//        Debug.Log("Backward speed: " + this.rotationSpeed);
//        this.forwardSpeedRelation = this.forwardSpeed / this.ball.speed;
//        Debug.Log("Forward speed: " + this.forwardSpeed);
        this.rb.angularVelocity = this.constantSpeed;

        this.determineTouchBorders ();

	}

    void determineTouchBorders () 
    {
        if (SystemInfo.deviceType == DeviceType.Handheld) 
        {
            int width = Screen.width;
            this.westTouchBorder = width / 3;
            this.eastTouchBorder = width - this.westTouchBorder;
        }
    }

    public static Rotate GetRotate () 
    {
        GameObject rotationObject = GameObject.FindGameObjectWithTag("bar");
        if (rotationObject != null) 
        {
            return rotationObject.GetComponent<Rotate> ();
        } else 
        {
            return null;
        }
    }
	
	// Update is called once per frame
	void Update () 
    {
        if (SystemInfo.deviceType == DeviceType.Handheld) 
        {
            if (Input.touchCount == 1) 
            {
                Touch touch = Input.GetTouch(0);
                Vector2 location = touch.position;
                if (location.x < this.westTouchBorder) 
                {
                    if (GlobalParams.InverseControls)
                        this.onArrowLeft ();
                    else
                        this.onArrowRight ();
                } else if (location.x > this.eastTouchBorder) 
                {
                    if (GlobalParams.InverseControls)
                        this.onArrowRight ();
                    else
                        this.onArrowLeft ();
                } else 
                {
                    this.noChanges ();
                }
            } else 
            {
                this.noChanges ();
            }
        } else 
        {
            if (Input.GetKey(KeyCode.LeftArrow)) 
            {
                if (GlobalParams.InverseControls) 
                {
                    this.onArrowRight ();
                    Debug.Log("Inverse");
                }
                else
                    onArrowLeft();
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                if (GlobalParams.InverseControls)
                    this.onArrowLeft ();
                else
                    onArrowRight();
            }
            
            else
            {
                noChanges();
            }
        }

        
	}

    void noChanges()
    {
        rb.angularVelocity = this.constantSpeed;
    }
     void onArrowLeft() 
    {
        rb.angularVelocity = this.constantSpeed + this.forwardSpeed;       
       
    }

     void onArrowRight() {
        rb.angularVelocity = this.constantSpeed - this.rotationSpeed;     
    }
}
