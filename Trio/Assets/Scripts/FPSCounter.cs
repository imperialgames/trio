﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FPSCounter : MonoBehaviour 
{
    public Text counterText;
    public float updateRate;

    private int frameCount;
    private float dt;
    private float fps;

	void Start () 
    {
        this.frameCount = 0;
        this.dt = 0.0f;
        this.fps = 0.0f;
	}
	
	void Update () 
    {
        this.frameCount++;
        this.dt += Time.deltaTime;
        if (dt > 1.0f / this.updateRate) 
        {
            this.fps = (float) this.frameCount / this.dt;
            this.frameCount = 0;
            this.dt -= 1.0f / updateRate;
            this.counterText.text = Mathf.RoundToInt(this.fps).ToString();
        }
	}
}
