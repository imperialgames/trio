﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuController : MonoBehaviour 
{
    public Camera gameCamera;
    public GameObject background;
    public Text highScoreLabel;
    public Text buttonText;
    public Text titleText;
    public static Sprite currentBackground;

    public Text inverseButtonText;
    public Button inverseButton;
    public Text soundButtonText;
    public Button soundButton;

    private Settings settings;
    
    public float ScreenHeight
    {
        get 
        {
            
            return gameCamera.orthographicSize * 2.0f;
        }
    }
    
    public float ScreenWidth 
    {
        get 
        {
            float aspectRatio = (float) Screen.width / (float) Screen.height;
            return this.ScreenHeight * aspectRatio;
        }
    }

	
	void Start () 
    {
        Application.targetFrameRate = -1;
        this.instantiateBackground ();
        this.highScoreLabel.text = "High Score: " + HighScore.Load ();
        this.settings = Settings.Load ();
        if (this.settings != null) 
        {
            GlobalParams.Sound = this.settings.sound;
            GlobalParams.InverseControls = this.settings.inverse;

        } else 
        {
            GlobalParams.Sound = true;
            GlobalParams.InverseControls = false;
            this.settings = new Settings();
            settings.sound = true;
            settings.inverse = false;
        }
        if (GlobalParams.InverseControls) 
        {
            this.inverseButtonText.text = "Inverse controls: ON";
        } else 
        {
            this.inverseButtonText.text = "Inverse controls: OFF";
        }
        this.checkSound ();
        this.StartCoroutine (this.layoutUI ());

	}

    void checkSound () 
        
    {
        if (GlobalParams.Sound) 
        {
            this.soundButtonText.text = "Sound: ON";
        } else 
        {
            this.soundButtonText.text = "Sound: OFF";
        }
    }

    IEnumerator layoutUI () 
    {
        yield return 0;

        RectTransform inverseButtonRectTransform = this.inverseButton.GetComponent<RectTransform> ();
        RectTransform soundButtonRectTransform = this.soundButton.GetComponent<RectTransform> ();
        soundButtonRectTransform.anchoredPosition = new Vector2(0.0f, - inverseButtonRectTransform.rect.height);
    }



    void instantiateBackground () 
    {
        GameObject background = (GameObject) Instantiate(this.background, new Vector3(0.0f, 0.0f, 1.0f), Quaternion.identity);
        Sprite[] backgrounds = Resources.LoadAll<Sprite>("Backgrounds");      
        SpriteRenderer renderer = background.GetComponent<SpriteRenderer> ();
        renderer.sprite = backgrounds[Random.Range(0, backgrounds.GetLength(0))];
        currentBackground = renderer.sprite;
        float width = renderer.sprite.rect.width / renderer.sprite.pixelsPerUnit;
        float height = renderer.sprite.rect.height / renderer.sprite.pixelsPerUnit;
        float xScale = this.ScreenWidth / width;
        float yScale = this.ScreenHeight / height;
        if (xScale >= yScale) 
        {
            background.transform.localScale= new Vector3(xScale, xScale, 1.0f);
        } else 
        {
            background.transform.localScale= new Vector3(yScale, yScale, 1.0f);
        }
    }
	
    public void StartGame() 
    {
        Application.LoadLevel(1);
    }

    public void ChangeControls () 
    {
        if (GlobalParams.InverseControls) 
        {
            GlobalParams.InverseControls = false;
            this.inverseButtonText.text = "Inverse controls: OFF";
            this.settings.inverse = false;
            Settings.Save(this.settings);
        } else 
        {
            GlobalParams.InverseControls = true;
            this.inverseButtonText.text = "Inverse controls: ON";
            this.settings.inverse = true;
            Settings.Save(this.settings);

        }
    }

    public void ToggleSound () 
    {
        if (GlobalParams.Sound) 
        {
            GlobalParams.Sound = false;
            this.soundButtonText.text = "Sound: OFF";
            this.settings.sound = false;
            Settings.Save(this.settings);

        } else 
        {
            GlobalParams.Sound = true;
            this.soundButtonText.text = "Sound: ON";
            this.settings.inverse = true;
            Settings.Save(this.settings);

        }
    }
	
}
