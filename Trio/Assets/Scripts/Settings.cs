﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[System.Serializable]
public class Settings 
{
    public bool sound;
    public bool inverse;

    public static void Save(Settings settings) 
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create (Application.persistentDataPath + "/savedGames.gd");
        bf.Serialize(file, settings);
        file.Close();
    }

    public static Settings Load() 
    {
        if (File.Exists(Application.persistentDataPath + "/savedGames.gd")) 
        {
            BinaryFormatter bf = new BinaryFormatter ();
            
            FileStream file = File.Open(Application.persistentDataPath + "/savedGames.gd", FileMode.Open);
            if (file.Length > 0) 
            {
                Settings settings = (Settings) bf.Deserialize(file);
                file.Close ();
                return settings;
                
            } else 
            {
                file.Close ();
                return null;
            }
        } else 
        {
            return null;
        }

    }
}
