﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;

public static class HighScore 
{


    public static void Save(int score) 
    {
        BinaryFormatter bf = new BinaryFormatter ();
        FileStream file = File.Create (Application.persistentDataPath + "/highscore.gd");
        bf.Serialize (file, score);
        file.Close ();
    }

    public static int Load () 
    {
            
        if (File.Exists(Application.persistentDataPath + "/highscore.gd")) 
        {
            BinaryFormatter bf = new BinaryFormatter ();

            FileStream file = File.Open(Application.persistentDataPath + "/highscore.gd", FileMode.Open);
            if (file.Length > 0) 
            {
                int highScore = (int) bf.Deserialize(file);
                file.Close ();
                return highScore;

            } else 
            {
                file.Close ();
                return 0;
            }
        } else 
        {
            return 0;
        }

    }
}
